from django.urls import path
from receipts.views import (
    show_receipts,
    create_form,
    category_list,
    account_list,
    create_expense,
    create_account,
)

urlpatterns = [
    path("", show_receipts, name="home"),
    path("create/", create_form, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("categories/create/", create_expense, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
