from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import CreateReceiptForm, CreateExpense, CreateAccountForm


# Create your views here.
@login_required
def show_receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts": receipts}
    return render(request, "receipts/home.html", context)


@login_required
def create_form(request):
    if request.method == "POST":
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.purchaser = request.user
            category.save()
            return redirect("home")
    else:
        form = CreateReceiptForm()
    context = {"form": form}
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {"category": category}
    return render(request, "receipts/categories.html", context)


def account_list(request):
    account_amount = Account.objects.filter(owner=request.user)
    context = {"account_amount": account_amount}
    return render(request, "receipts/accounts.html", context)


@login_required
def create_expense(request):
    if request.method == "POST":
        form = CreateExpense(request.POST)
        if form.is_valid():
            x = form.save(False)
            x.owner = request.user
            x.save()
            return redirect("category_list")

    else:
        form = CreateExpense()
    context = {"form": form}
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            x = form.save(False)
            x.owner = request.user
            x.save()
            return redirect("account_list")
    else:
        form = CreateAccountForm()
    context = {"form": form}
    return render(request, "receipts/create_account.html", context)
