from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(max_length=150, required=False)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)


class SignUp(forms.Form):
    username = forms.CharField(max_length=150, required=False)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)
    password_confirmation = forms.CharField(
        max_length=150, widget=forms.PasswordInput
    )
